package com.zh.class001;

import java.util.HashMap;
import java.util.Map;

/**
 * 一个数组中有一种数出现K次，其他数都出现了M次，M>1，K<M找到，
 * 出现了K次的数，要求，额外空间复杂度O(1)，时间复杂度O(N)
 *
 */
public class KTimesNum {
    private static final int K = 1;
    private static final int M = 3;
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 2, 2, 2, 3, 3, 3, 0, 0};
        System.out.println(onlyKTimesNUms(arr, K, M));
        System.out.println(test(arr, K, M));
    }
    public static int onlyKTimesNUms(int[] arr, int k, int m) {
        int[] bitSum = new int[32];

        for (int num : arr) {
            for (int i = 0; i < 32; i++) {
                if (((num >> i) & 1) != 0) {
                    bitSum[i]++;
                }
            }
        }

        int res = 0;
        for (int i = 0; i < 32; i++) {
            if (bitSum[i] % m == 0) {
                continue;
            }
            if (bitSum[i] % m == k) {
                res |= (1 << i);
            } else {
                return -1;
            }
        }

        int zeroCount = 0;
        if (res == 0) {
            for (int num : arr) {
                if (num == 0) {
                    zeroCount++;
                }
            }

            if (zeroCount != k) {
                return -1;
            }
        }
        return res;
    }

    public static int test(int[] arr, int k, int m) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : arr) {
            if (map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
        }

        for (int key : map.keySet()) {
            if (map.get(key) == k) {
                return key;
            }
        }
        return -1;

    }
}
