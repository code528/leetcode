package com.zh.class001;

public class InsertSort {
    public static int[] insertSort(int[] array) {
        if (array == null || array.length < 2) {
            return array;
        }
        for (int i = 1; i < array.length; i++) {
            int curVal = array[i];
            int preIndex = i - 1;
            //找到第一个比curVal小的数据，其他的数据不断后移
            while (preIndex >= 0 && curVal < array[preIndex]) {
                array[preIndex + 1] = array[preIndex];
                preIndex--;
            }
            array[preIndex + 1] = curVal;
        }
        return array;
    }
    public static void main(String[] args) {
        int[] arr = {8, 4, 9, 2, 1, 6, 3, 7, 5, 8};
        System.out.println("排序前：");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
        System.out.println("排序后：");
        insertSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
