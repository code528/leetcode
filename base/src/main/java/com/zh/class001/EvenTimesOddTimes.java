package com.zh.class001;

public class EvenTimesOddTimes {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 1, 2, 3, 4, 1};
        printOddTimesNum1(arr);

        int[] arr1 = {1, 2, 3, 4, 1, 2, 3, 4, 1, 2};
        printOddTimesNum2(arr1);

        int i = bit1Count(8);
        System.out.println(i);
    }
    // arr中，只有一种数，出现奇数次
    public static void printOddTimesNum1(int[] arr) {
        int eor = 0;
        for (int i = 0; i < arr.length; i++) {
            eor ^= arr[i];
        }
        System.out.println(eor);
    }

    // arr中，有两种数，出现奇数次
    public static void printOddTimesNum2(int[] arr) {
        int eor = 0;
        for (int num : arr) {
            eor ^= num;
        }

        // 提取最右侧的1
        int rightOne = eor & (~eor + 1);
        int onlyOne = 0;
        for (int num : arr) {
            if ((num & rightOne) != 0) {
                onlyOne ^= num;
            }
        }
        System.out.println(onlyOne + " " + (eor ^ onlyOne));
    }

    // 怎么把一个int类型的数，提取出最右侧的1来
    public static int getRightOne(int num) {
        return num & (~num + 1);
    }

    public static int bit1Count(int num) {
        int count = 0;
        while (num != 0) {
            int rightOne = num & (~num + 1);
            count++;
            num ^= rightOne;
        }
        return count;
    }

}
