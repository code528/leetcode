package com.zh.class001;

import com.zh.util.SortUtil;

public class BinaryInsertionSort {
    public static int[] binaryInsertionSort(int[] array) {
        if (array == null || array.length == 0) {
            return array;
        }
        for (int i = 1; i < array.length; i++) {
            int curVal = array[i];
            int left = 0;
            int right = i - 1;

            //搜索有序区中第一个大于 current 的位置，即为 current 要插入的位置
            while (left <= right) {
                int mid = left + (right - left) / 2;
                if (array[mid] > curVal) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }

            for (int j = i - 1; j >= left; j --) {
                array[j + 1] = array[j];
            }
            array[left] = curVal;
        }
        return array;
    }

    public static void main(String[] args) {
        int[] arr = SortUtil.generateRandomArray(100, 100);
        int[] arr1 = SortUtil.copyArray(arr);
        boolean equal = SortUtil.isEqual(arr, arr1);
        System.out.println(equal);
    }
}
