package com.zh.class001;

public class ShellSort {
    public static void main(String[] args) {
        int[] arr = {8, 4, 9, 2, 1, 6, 3, 7, 5, 8, 16, 12};
        System.out.println("排序前：");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
        System.out.println("排序后：");
        shellSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static int[] shellSort(int[] array) {
        if (array == null || array.length == 0) {
            return array;
        }

        int len = array.length;
        int gap = len >> 1;

        while (gap > 0) {
            for (int i = gap; i < len; i++) {
                int curVal = array[i];
                int preIndex = i - gap;
                while (preIndex >= 0 && array[preIndex] > curVal) {
                    array[preIndex + gap] = array[preIndex];
                    preIndex = preIndex - gap;
                }

                array[preIndex + gap] = curVal;
            }
            gap = gap >> 1;

        }
        return array;
    }

}
