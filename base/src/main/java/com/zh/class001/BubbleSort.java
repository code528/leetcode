package com.zh.class001;

import com.zh.util.SortUtil;

public class BubbleSort {
    public static int[] bubbleSort(int[] array) {
        if (array == null || array.length < 2) {
            return array;
        }
        int len = array.length;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len - i - 1; j++) {
                //如果前一个数组比后面的大，交换位置，经过一趟排序，会把大的数字上浮到后面
                if (array[j] > array[j+1]) {
                    SortUtil.swap(array, j, j+1);
                }
            }
        }
        return array;
    }

    public static void main(String[] args) {
        int[] arr = {8, 4, 9, 2, 1, 6, 3, 7, 5, 8};
        System.out.println("排序前：");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
        System.out.println("排序后：");
        bubbleSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
