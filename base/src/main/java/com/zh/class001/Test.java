package com.zh.class001;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int a = 10;
        int b = -100;

//        swap(a, b);

        int[] arr = {3, 1, 100};
        swap(arr, 0, 0);
        System.out.println(Arrays.toString(arr));

    }

    public static void swap(int[] arr, int i, int j) {
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

    public static void swap(int a, int b) {
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println("a:" + a);
        System.out.println("b:" + b);
    }
}
