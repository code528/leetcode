package com.zh.class001;

import com.zh.util.SortUtil;

public class SelectionSort {
    public static int[] selectionSort(int[] array) {
        if (array == null || array.length < 2) {
            return array;
        }
        int len = array.length;
        for (int i = 0; i < len - 1; i++) {
            int minIndex = i;
            //每次找到最小值，插入有序数列中
            for (int j = i + 1; j < len; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            SortUtil.swap(array, minIndex, i);
        }
        return array;
    }

    public static void main(String[] args) {
        int[] arr = {8, 4, 9, 2, 1, 6, 3, 7, 5, 8};
        System.out.println("排序前：");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
        System.out.println("排序后：");
        selectionSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
