package com.zh.class002.stackandqueue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Test {
    public static void main(String[] args) {
        int oneTestDataNum = 100;
        int value = 10000;
        int testTimes = 100000;
        MyStack<Integer> myStack = new MyStack<>();
        Stack<Integer> stack = new Stack<>();
        MyQueue<Integer> myQueue = new MyQueue<>();
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < testTimes; i++) {
            for (int j = 0; j < oneTestDataNum; j++) {
                int nums = (int) (Math.random() * value);
                myStack.push(nums);
                stack.push(nums);
                myQueue.push(nums);
                queue.add(nums);
            }
        }

        while (!stack.isEmpty() && !myStack.isEmpty()) {
            if (!isEqual(stack.pop(), myStack.pop())) {
                System.out.println("mystack is not true");
            }
        }

        while (!queue.isEmpty() && !myQueue.isEmpty()) {
            if (!isEqual(queue.poll(), myQueue.poll())) {
                System.out.println("myQueue is not true");
            }
        }

        System.out.println("finish!");
    }

    public static boolean isEqual(Integer o1, Integer o2) {
        if (o1 == null && o2 != null) {
            return false;
        }
        if (o1 != null && o2 == null) {
            return false;
        }
        if (o1 == null && o2 == null) {
            return true;
        }
        return o1.equals(o2);
    }
}
