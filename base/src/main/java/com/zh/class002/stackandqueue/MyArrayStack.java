package com.zh.class002.stackandqueue;

public class MyArrayStack {
    public static void main(String[] args) {
        MyArrayStack myArrayStack = new MyArrayStack(3);
        myArrayStack.push(1);
        myArrayStack.push(2);
        myArrayStack.push(3);
        System.out.println(myArrayStack.pop());
        myArrayStack.push(4);
        System.out.println(myArrayStack.pop());

    }
    private int[] arr;
    int index = 0;
    private final int limit;

    public MyArrayStack(int limit) {
        arr = new int[limit];
        this.limit = limit;
    }

    public void push(int value) {
        if (index == limit) {
            throw new RuntimeException("栈满了，不能再加了");
        }
        arr[index ++] = value;
    }

    public int pop() {
        if (index == 0) {
            throw new RuntimeException("栈空了，不能再拿了");
        }
        return arr[--index];
    }

    public boolean isEmpty() {
        return index == 0;
    }
}
