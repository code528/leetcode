package com.zh.class002.stackandqueue;

import com.zh.class002.DoubleNode;

public class DoubleEndsQueue<T> {
    DoubleNode<T> head = null;
    DoubleNode<T> tail = null;
    public void addFromHead(T value) {
        DoubleNode<T> cur = new DoubleNode<>(value);
        if (head == null) {
            head = cur;
            tail = cur;
        } else {
            cur.next = head;
            head.pre = cur;
            head = cur;
        }
    }
    public void addFromBottom(T value) {
        DoubleNode<T> cur = new DoubleNode<>(value);
        if (head == null) {
            head = cur;
            tail = cur;
        } else {
            tail.next = cur;
            cur.pre = tail;
            tail = cur;
        }
    }

    public T popFromHead() {
        if (head == null) {
            return null;
        }
        DoubleNode<T> cur = head;
        if (head == tail) {
            head = null;
            tail = null;
        } else {
            head = head.next;
            cur.next = null;
            head.pre = null;
        }
        return cur.value;
    }

    public T popFromBottom() {
        if (head == null) {
            return null;
        }
        DoubleNode<T> cur = tail;
        if (head == tail) {
            head = null;
            tail = null;
        } else {
            tail = tail.pre;
            tail.next = null;
            cur.pre = null;
        }
        return cur.value;
    }

    public boolean isEmpty() {
        return head == null;
    }
}
