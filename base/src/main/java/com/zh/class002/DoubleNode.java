package com.zh.class002;

public class DoubleNode<T> {
    public T value;
    public DoubleNode<T> pre;
    public DoubleNode<T> next;

    public DoubleNode(T value) {
        this.value = value;
    }
}
