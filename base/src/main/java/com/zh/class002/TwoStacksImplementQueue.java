package com.zh.class002;

import java.util.Stack;

public class TwoStacksImplementQueue {
    public static void main(String[] args) {
        TwoStacksImplementQueue twoStacksImplementQueue = new TwoStacksImplementQueue();
        twoStacksImplementQueue.add(1);
        twoStacksImplementQueue.add(2);
        twoStacksImplementQueue.add(3);
        System.out.println(twoStacksImplementQueue.poll());
        System.out.println(twoStacksImplementQueue.poll());
        twoStacksImplementQueue.add(5);
        System.out.println(twoStacksImplementQueue.poll());
        System.out.println(twoStacksImplementQueue.poll());

    }
    Stack<Integer> stackPush;
    Stack<Integer> stackPop;

    public TwoStacksImplementQueue() {
        this.stackPush = new Stack<>();
        this.stackPop = new Stack<>();
    }

    public void add(int pushInt) {
        stackPush.push(pushInt);
        pushToPop();
    }

    private void pushToPop() {
        if (stackPop.isEmpty()) {
            while (!stackPush.isEmpty()) {
                stackPop.push(stackPush.pop());
            }
        }
    }

    public int poll() {
        if (stackPop.isEmpty() && stackPush.isEmpty()) {
            throw new RuntimeException("Queue is empty!");
        }
        pushToPop();
        return stackPop.pop();
    }

    public int peek() {
        if (stackPop.isEmpty() && stackPush.isEmpty()) {
            throw new RuntimeException("Queue is empty!");
        }
        pushToPop();
        return stackPop.peek();
    }
}
