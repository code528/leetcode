package com.zh.class002;

public class DeleteGivenValue {
    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(2);
        head.next.next.next = new Node(3);
        Node node = removeValue(head, 2);

        System.out.println(node);
    }
    public static Node removeValue(Node<Integer> head, int num) {
        // head来到第一个不需要删的位置
        while (head != null) {
            if (head.value == num) {
                head = head.next;
            } else {
                break;
            }
        }

        Node<Integer> pre = head;
        Node<Integer> cur = head;
        while (cur != null) {
            if (cur.value == num) {
                pre.next = cur.next;
            } else {
                pre = cur;
            }
            cur = cur.next;
        }
        return head;
    }
}
