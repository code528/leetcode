package com.zh.class002;

import java.util.Stack;

public class GetMinStack {
    public static void main(String[] args) {
        GetMinStack getMinStack = new GetMinStack();
        getMinStack.push(1);
        getMinStack.push(2);
        getMinStack.push(3);
        System.out.println(getMinStack.getmin());
        getMinStack.pop();
        System.out.println(getMinStack.getmin());
        getMinStack.pop();
        System.out.println(getMinStack.getmin());
    }
    Stack<Integer> dataStack;
    Stack<Integer> minStack;

    public GetMinStack() {
        this.dataStack = new Stack<>();
        this.minStack = new Stack<>();
    }

    public void push(int newNum) {
        if (minStack.isEmpty()) {
            minStack.push(newNum);
        } else if (newNum < minStack.peek()) {
            minStack.push(newNum);
        } else {
            int newMin = minStack.peek();
            minStack.push(newMin);
        }
        dataStack.push(newNum);
    }

    public int pop() {
        if (this.dataStack.isEmpty()) {
            throw new RuntimeException("Your stack is empty.");
        }
        this.minStack.pop();
        return this.dataStack.pop();
    }

    public int getmin() {
        if (this.minStack.isEmpty()) {
            throw new RuntimeException("Your stack is empty.");
        }
        return this.minStack.peek();
    }
}
