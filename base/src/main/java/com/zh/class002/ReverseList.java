package com.zh.class002;

public class ReverseList {
    public static void main(String[] args) {
        Node<Integer> head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        Node<Integer> node = reverseLinkedList(head);
        System.out.println(node.value);
        System.out.println(node.next.value);
        System.out.println(node.next.next.value);
    }
    public static Node<Integer> reverseLinkedList(Node<Integer> head) {
        Node<Integer> pre = null;
        Node<Integer> next = null;

        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    public static DoubleNode reverseDoubleLinkedList(DoubleNode head) {
        DoubleNode pre = null;
        DoubleNode next = null;

        while (head != null) {
            next = head.next;

            head.next = pre;
            head.pre = next;

            pre = head;
            head = next;
        }
        return pre;
    }
}
